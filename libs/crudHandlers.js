var mongoose = require('mongoose');
var db = require('../database');

module.exports = function (modelName) {

    // Список документов
    var list = function (req, reply) {
        console.log(modelName);
        db.model(modelName).find({}, function (err, data) {
            if (err) return reply('Not found.').code(404);
            return reply(data);
        });
    };

    // Один документ
    var get = function (req, reply) {
        console.log(req, reply);
        try{var id = mongoose.Types.ObjectId(req)}
        catch (e){reply('Not found.').code(404)}

        db.model(modelName).find({_id: id}, function (err, data) {

            if (err) return reply('Not found.').code(404);

            if (data) {
                console.log(err, data);
                reply(data);
            } else {
                reply(err).code(404);
            }
        })
    };

    // Создаем документ
    var create = function (req, reply) {
        db.model(modelName).create(req.payload, function (err, data) {
            if (err) return reply(err.errors).code(400);
            reply(data);
        });
    };

    // Обновляем документ
    var update = function (req, reply) {
        try{var id = mongoose.Types.ObjectId(req.payload.id)}
        catch (e){reply(400)}

        db.model(modelName).findByIdAndUpdate({_id: id}, {$set: req.payload}, function (err, numberAffected, data) {
            if (err) return reply('Not found.').code(404);

            if (numberAffected) {
                reply(200);
            } else {
                reply(404);
            }

        })
    };

    // Удаляем документ
    var remove = function (req, reply) {
        try{var id = mongoose.Types.ObjectId(req.payload.id)}
        catch (e){reply(400)}

        db.model(modelName).remove({_id: id}, function (err, data) {
            if (err) return reply('Not found.').code(404);

            reply(data ? req.payload.id : 404);
        });
    };

    return {
        list  : list,
        get   : get,
        create: create,
        update: update,
        remove: remove
    }
};