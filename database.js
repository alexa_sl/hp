var Mongoose = require('mongoose');
var Config = require('./config');
var fs = require('fs');
var path = require('path');
var async = require('async');

//load database
// Mongoose.connect('mongodb://localhost/test');
Mongoose.connect('mongodb://' + Config.mongo.username + ':' + Config.mongo.password + '@' + Config.mongo.url + '/' + Config.mongo.database);
var db = Mongoose.connection;

db.on('error', function (err) {
    // Обрабатываем ошибку
    console.log('info', 'Connection error');
    //log.error('mongoose error!');
});
db.once('open', function callback() {
    // Соединение прошло успешно
    //log.info('mongoose connected!');
    console.log('mongoose connected!');
});

var models = {};

//Инициализируем все схемы
var init = function (modelsDirectory, callback) {
    //Считываем список файлов из modelsDirectory
    var schemaList = fs.readdirSync(modelsDirectory);
    console.log('schemaList', schemaList);
    //Создаем модели Mongoose и вызываем callback, когда все закончим
    async.each(schemaList, function (item, cb) {
        var modelName = path.basename(item, '.js');
        models[modelName] = require(path.join(modelsDirectory, modelName))(Mongoose);
        cb();
    }, callback);
};

//Возвращаем уже созданные модели из списка
var model = function (modelName) {
    var name = modelName.toLowerCase();
    if (typeof models[name] == "undefined") {
        // Если модель на найдена, то создаем ошибку
        throw "Model '" + name + "' is not exist";
    }
    return models[name];
};

module.exports.init = init;
module.exports.model = model;