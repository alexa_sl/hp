var Hapi = require('hapi');
var Good = require('good');
var Bcrypt = require('bcrypt');
var Basic = require('hapi-auth-basic');

var server = new Hapi.Server();
var db = require('./database');
var path = require('path');
var winston = require('winston');
var host = 'frozen-spire-1368.herokuapp.com';

server.connection({ host: 'frozen-spire-1368.herokuapp.com'});
console.log(server.info);

var handlers = {
    entities: require('./handlers/entities'),
    questions: require('./handlers/questions')
};


server.route({
    method: 'GET',
    path: '/',
    handler: function (request, reply) {
        reply('Hello, world!');
    }
});

server.route({
    method: 'GET',
    path: '/{name}',
    handler: function (request, reply) {
        reply('Hello, ' + encodeURIComponent(request.params.name) + '!');
    }
});

server.route({
    method: 'GET',
    path: '/api',
    handler: function (request, reply) {
        reply('Api runs!');
    }
});

var users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret'
        name: 'John Doe',
        id: '2133d32a'
    }
};

var validate = function (username, password, callback) {
    var user = users[username];
    if (!user) {
        return callback(null, false);
    }

    Bcrypt.compare(password, user.password, function (err, isValid) {
        callback(err, isValid, { id: user.id, name: user.name });
    });
};

server.register({
    register: Good,
    options: {
        reporters: [{
            reporter: require('good-console'),
            args:[{ log: '*', response: '*' }]
        }]
    }
}, function (err) {
    if (err) {
        throw err; // something bad happened loading the plugin
    }

    server.start(function () {
        server.log('info', 'Server running at: ' + server.info.uri);
    });
});

server.register(Basic, function (err) {
    server.auth.strategy('simple', 'basic', { validateFunc: validate });

    server.route({
        method: 'GET',
        path: '/logs',
        handler: handlers.entities.list
    });

    server.route({
        method: 'GET',
        path: '/log/{id?}',
        handler: function (req, reply) {
            if (req.params.id) {
                handlers.entities.get(req.params.id, reply);
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/post',
        config: {
            auth: 'simple',
            handler: handlers.entities.create
        }
    });

    server.route({
        method: 'PUT',
        path: '/put',
        config: {
            auth: 'simple',
            handler: handlers.entities.update
        }
    });

    server.route({
        method: 'DELETE',
        path: '/delete',
        config: {
            auth: 'simple',
            handler: handlers.entities.remove
        }
    });


    // questions routes
    server.route({
        method: 'GET',
        path: '/questions',
        handler: handlers.questions.list
    });

    server.route({
        method: 'GET',
        path: '/question/{id?}',
        handler: function (req, reply) {
            if (req.params.id) {
                handlers.questions.get(req.params.id, reply);
            }
        }
    });


    server.route({
        method: 'POST',
        path: '/questions',
        config: {
            auth: 'simple',
            handler: handlers.questions.create
        }
    });

    server.route({
        method: 'PUT',
        path: '/questions',
        config: {
            auth: 'simple',
            handler: handlers.questions.update
        }
    });

    server.route({
        method: 'DELETE',
        path: '/questions',
        config: {
            auth: 'simple',
            handler: handlers.questions.remove
        }
    });


});

//setTimeout(function() {
    db.init(path.join(__dirname, "models"), function (err, data) {
        //Выводим сообщение об успешной инициализации базы данных
        winston.info("All the models are initialized", data);
    });
//}, 2000);
